RSpec.describe Pulsus::Second do
  describe "#long_chars" do
    let!(:word) { "Professor" }
    subject { described_class.new(word).long_chars }

    it "returns the char with appears more times consecutive" do
      is_expected.to eq("ss")
    end

    context "when don't have repeated chars" do
      let!(:word) { "Pulsus" }

      it { is_expected.to eq("") }
    end

    context "when have 2 or more chars with same count" do
      let!(:word) { "Amarrassem" }

      it "returns the last chars of word" do
        is_expected.to eq("ss")
      end
    end
  end
end
