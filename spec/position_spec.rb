RSpec.describe Pulsus::Position do
  describe "#count!" do
    subject { described_class.new(char: "a", index: 0) }

    it "increments #count by 1" do
      expect{ subject.count! }.to change(subject, :count).by(1)
    end
  end
end
