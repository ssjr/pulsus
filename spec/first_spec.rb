RSpec.describe Pulsus::First do
  describe "#positions_of_bigger_range" do
    subject { described_class.new(list).positions_of_bigger_range }

    context "empty list" do
      let(:list) { [] }

      it { is_expected.to be_nil }
    end

    context "list with one element" do
      let(:list) { [20] }

      it { is_expected.to eq([0, 0]) }
    end

    context "list with 2+ elements" do
      context "with negative and positive numbers" do
        let(:list) { [2, -4, 6, 8, -10, 100, -6, 5] }

        it "returns the index of bigger sum range" do
          is_expected.to eq([2, 5])
        end

        context "when one is positive and one negative" do
          let(:list) { [2, -2] }

          it "returns the bigger index" do
            is_expected.to eq([0, 0])
          end
        end

        context "when have 2 possible ranges" do
          let(:list) { [-2, 0, 6, 8, -10, 100, -6, 5] }

          it "returns the first result with index closest to 0" do
            is_expected.to eq([1, 5])
          end
        end
      end

      context "only negative numbers in list" do
        let(:list) { [-5, -6, -4, -7, -8] }

        it "returns the index of bigger value" do
          is_expected.to eq([2, 2])
        end
      end

      context "only positive numbers in list" do
        let(:list) { [5, 6, 7, 8, 9] }

        it "returns 0 and size - 1" do
          is_expected.to eq([0, list.size - 1])
        end
      end
    end
  end
end
