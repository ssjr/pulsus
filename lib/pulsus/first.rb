module Pulsus
  class First
    attr_accessor :list, :min_index, :max_index

    def initialize(list)
      self.list = list
      if list.any?
        self.min_index = 0
        self.max_index = 0
      end
    end

    # Returns an array with initial and final positions of the range.
    # Returns nil if the input was empty.
    def positions_of_bigger_range
      if list.empty?
        nil
      else
        check_ranges
        [min_index, max_index]
      end
    end

    private

    # Set min and max index for maximum range sum
    def check_ranges
      list.each_with_index do |_, i|
        max_to_go = ((i)...(list.size)).to_a.reverse
        for j in max_to_go
          if list[i..j].sum > list[min_index..max_index].sum
            self.min_index = i
            self.max_index = j
          end
        end
      end
    end
  end
end
