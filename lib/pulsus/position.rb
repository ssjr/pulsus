module Pulsus
  class Position
    attr_accessor :char, :index, :count

    def initialize(char:, index:)
      self.char = char
      self.index = index
      self.count = 1
    end

    # Increment the counter
    def count!
      self.count += 1
    end
  end
end
