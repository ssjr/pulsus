require "pulsus/position"

module Pulsus
  class Second
    attr_accessor :word, :positions

    def initialize(word)
      self.word = word.downcase
      self.positions = @word.chars.each_with_index.map do |char, i|
        Position.new(char: char, index: i)
      end
    end

    # Returns the longest sequence of same character
    def long_chars
      count_repeat_chars
      remove_uniq_chars
      sort_by_count
      if position = positions.first
        position.char * position.count
      else
        ""
      end
    end

    private

    # See the number of times that same character appears in sequence
    def count_repeat_chars
      positions.map do |position|
        for i in ((position.index + 1)..positions.length)
          if positions[i] && positions[i].char == position.char
            position.count!
          else
            break
          end
        end
      end
    end

    # Remove positions with count == 1. We need just with 2+
    def remove_uniq_chars
      positions.select!{|position| position.count > 1}
    end

    # Order positions by number of times that character appears in sequence
    def sort_by_count
      positions.sort_by!(&:count).reverse!
    end
  end
end
